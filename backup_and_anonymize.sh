#!/usr/bin/env bash
#
# A. Koch, 2023 [CC4]
#
# Script to backup and anonymize moodle data
#
# Modifications and fixes by Andreas Grupp

# Source configuration for this script
source config.ini

# Make sure database and table for anonymization exists:
# CREATE TABLE users( 
#     origid int NOT NULL,
#     anonid int NOT NULL,
#     name CHAR(30) NOT NULL,
#     PRIMARY KEY(origid)
# );
time="$(date '+%Y%m%d%H%M%S')"

# Extract relevant data from origin database and adapt SQL-Statements for import to
# the target database
echo "Backing up tables from DB ${dborigin} with timestamp ${time}..."
mysqldump -h ${dbhost} ${dborigin} ${tables} > "${backupdir}/dbtemp-${time}.sql"
sed -i "s/INSERT INTO/INSERT IGNORE INTO/g" "${backupdir}/dbtemp-${time}.sql"

# Insert previously saved data to database and delete data from courses not relevant
echo "Storing to ${dbtarget}..."
mysql -h ${dbhost} --one-database ${dbtarget} < "${backupdir}/dbtemp-${time}.sql"
echo "Cleaning tables except entries for courses ${courses}..."
mysql -h ${dbhost} --database ${dbtarget} -N -s -e "DELETE FROM mdl_logstore_standard_log WHERE courseid NOT IN (${courses})"
mysql -h ${dbhost} --database ${dbtarget} -N -s -e "DELETE FROM mdl_feedback WHERE course NOT IN (${courses})"
mysql -h ${dbhost} --database ${dbtarget} -N -s -e "DELETE FROM mdl_feedback_completed WHERE feedback NOT IN (SELECT id FROM mdl_feedback)"
mysql -h ${dbhost} --database ${dbtarget} -N -s -e "DELETE FROM mdl_feedback_item WHERE feedback NOT IN (SELECT id FROM mdl_feedback)"
mysql -h ${dbhost} --database ${dbtarget} -N -s -e "DELETE FROM mdl_feedback_value WHERE completed NOT IN (SELECT id FROM mdl_feedback_completed)"

# Remove IP-addresses from data
echo "Clearing IPs..."
mysql -h ${dbhost} --database ${dbtarget} -N -s -e "UPDATE mdl_logstore_standard_log SET ip=''"

echo "Retrieving new users to be anonymized..."
newusers=`mysql -h ${dbhost} --database ${dbtarget} -N -s -e "SELECT DISTINCT userid FROM mdl_logstore_standard_log WHERE userid NOT IN (SELECT origid FROM users)"`
if [ -z "${newusers}" ]; then
  echo "No new users. Skipping..."
else
  count=`echo ${newusers} | tr " " "\n" | wc -l`
  echo "Creating random data for ${count} new users used for anonymization..."
  for i in $(echo $newusers); do
    newid=`mysql -h ${dbhost} --database ${dbtarget} -N -s -e "SELECT 1000000 + FLOOR(RAND() * 8999999) AS newid FROM mdl_logstore_standard_log WHERE \"newid\" NOT IN (SELECT anonid FROM users) LIMIT 1"`;
    newname=`mysql -h ${dbhost} --database ${dbtarget} -N -s -e "SELECT LEFT(UUID(),8) AS newid FROM mdl_logstore_standard_log WHERE \"newid\" NOT IN (SELECT name FROM users) LIMIT 1"`;
    mysql -h ${dbhost} --database ${dbtarget} -N -s -e "INSERT INTO users VALUES(\"${i}\",\"${newid}\",\"${newname}\")";
  done;
fi;

echo "Anonymizing user data..."
for i in $(mysql -h ${dbhost} --database ${dbtarget} -N -s -e "SELECT CONCAT(origid,',',anonid,',',name) FROM users"); do
 oid=`echo $i | cut -d "," -f1`;
 aid=`echo $i | cut -d "," -f2`;
 name=`echo $i | cut -d "," -f3`;
 mysql -h ${dbhost} --database ${dbtarget} -N -s -e "UPDATE mdl_logstore_standard_log SET other=REGEXP_REPLACE(other, 'username\":\".*\"', 'username\":\"${name}\"') WHERE userid=\"${oid}\" AND userid<1000000 AND other LIKE \"%username%\"";
 mysql -h ${dbhost} --database ${dbtarget} -N -s -e "UPDATE mdl_logstore_standard_log SET userid=\"${aid}\" WHERE userid=\"${oid}\" AND userid<1000000";
 mysql -h ${dbhost} --database ${dbtarget} -N -s -e "UPDATE mdl_feedback_completed SET userid=\"${aid}\" WHERE userid=\"${oid}\" AND userid<1000000";
done;

echo "Creating backups of ${dbtarget}..."
mysqldump -h ${dbhost} ${dbtarget} ${tables} > "${backupdir}/db_backup-${time}.sql"
mysqldump -h ${dbhost} ${dbtarget} users > "${backupdir}/db_backup_users-${time}.sql"

echo "Compressing backups..."
zip -rm "${backupdir}/db_backup-${time}.zip" "${backupdir}/db_backup-${time}.sql"
zip -rm "${backupdir}/db_backup_users-${time}.zip" "${backupdir}/db_backup_users-${time}.sql"

echo "Cleaning up temp file..."
rm -f "${backupdir}/dbtemp-${time}.sql"

echo "Finished."
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "!!!    ONLY HAND OUT db_backup-${time}.* TO CUSTOMER   !!!"
echo "!!!      NEVER HAND OUT db_backup_users-${time}.*      !!!"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"

