# utueanonymize

Author: Andreas Koch
Date: 2023/08/05
Version: 1.0
License: CC-by-sa 4.0

This script exports data of some courses in a Moodle database,
stores it to another database and anonymizes it.

It is used for a project with a university to gather some user
data in an anonymous way.

## Installation

1. Adjust settings in .my.cnf and store it to your home folder.
2. Copy `config.ini.sample` to `config.ini` or check whether there are new settings compared to your already existing configuration
3. Adjust dborigin, dbtarget, courses, tables and backupdir in config-file 'config.ini'.
4. Create cronjob.

